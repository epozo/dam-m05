import java.util.*;
//Versi� final

public class Memory {

	static Random rnd = new Random();
	static Scanner reader=new Scanner(System.in);

	public static void main(String[] args) {
		joc();
	}

	public static void iniciarTauler(char[][] tauler, int midatauler){
			int i = 0, j = 0;
			for (i = 0;i<midatauler;i++) {
				for (j = 0;j<midatauler; j++) {
					tauler[i][j] = '?';
				}
			}
		}

	public static void mostrarTauler(char[][] tauler, int midatauler) {
		int i = 0, j = 0, num = 1;
		System.out.print(" ");
		System.out.print(" 1 ");
		System.out.print("2 ");
		System.out.print("3 ");
		System.out.println("4");

		for (i = 0; i<midatauler;i++) {
			System.out.print(num + " ");
			for (j = 0; j<midatauler; j++) {
				System.out.print(tauler[i][j] + " ");
				if (num == 4) {
					num = 0;
				}
			}
			System.out.println("");
			num++;
		}
	}

	public static void posarPeces(char[][] secret, int midatauler){
		int i = 0, j = 0, cont = 1, posicio = 0;
		char[] lletres = new char[]{'A','B','C','D','E','F','G','H'};
		for (i = 0; i < midatauler; i++) {
			for (j = 0; j < midatauler; j++) {
				if (cont == 1) {
					secret[i][j] = lletres[posicio];
					cont++;
				}else {
						secret[i][j] = lletres[posicio];
						cont = 1;
						posicio++;
				}
			}
		}
	}

	

	public static void PosaPecesV2(char taula[][], int a) {
    int i=0;
    int j=0;
    char letra='A';
	for(i=0; i<a; i++) {
		for (j=0; j<a; j=j+2) {
			taula[i][j]=letra;
			taula[i][j+1]=letra;
			letra++;
			}
 		}
 	}

 	static void posaPecesV3 (char matriz [] [], int max) {
		int i=0;
		int j=0;
		char letra='A';
		int cont=0;
		boolean hola=false;

		for(i=0;i<max;i++) {
			cont=0;
			for(j=0;j<max;j++) {

				if(hola==true) {
					cont=0;
				}
				hola=false;

				matriz[i][j]=letra;
				cont++;
				if(cont==2) {
					hola=true;
					letra++;
				}
			}
		}
 	}



	public static void remenarpeces(char[][] matriu, int midatauler){
		int num=1, fila=0, columna=0;
		char aux;
		for(int i=0;i<midatauler;i++) {
			for(int j=0;j<midatauler;j++) {
				fila=rnd.nextInt(midatauler);
				columna=rnd.nextInt(midatauler);
				aux=matriu[i][j];
				matriu[i][j]=matriu[fila][columna];
				matriu[fila][columna]=aux;
			}
		}
	}

	public static void iniciaSecret(char[][] matriu, int midatauler){
		posarPeces(matriu, midatauler);
		remenarpeces(matriu, midatauler);
	}

	public static int validarDada(int midatauler){
		boolean correcte=false;
		int nombre = 0;
		while(!correcte) {
            try {
                nombre=reader.nextInt();
                nombre=nombre-1;
                if(nombre<0|| nombre>=midatauler) {
                    System.out.println("Error! Nombre no valid");
                }else {
                    correcte=true;
                }
            }catch(Exception e) {
                System.out.println("Error! Nombre no valid");
                reader.nextLine();
            }
		}

		return nombre;
	}

	public static boolean validarCasella (char[][] tauler, int fila, int column){
			if(tauler[fila][column]!='?') {
				System.out.println("La casella ja esta destapada!");
				return false;
			}else {
				return true;
		}
	}

	public static boolean torn(char[][] tauler, char[][] secret, int midatauler){
		int fil=0,col=0;
		int fila=0,column=0;
		boolean continua;

		do {
            System.out.println("");
            System.out.println("Introdueix la fila de la casella que vols destapar: ");
            fila=validarDada(midatauler);
            System.out.println("Introdueix la columna de la casella que vols destapar: ");
            column=validarDada(midatauler);
		}
		while(!validarCasella(tauler, fila, column));

		tauler[fila][column]=secret[fila][column];
		fil=fila;
		col=column;
		mostrarTauler(tauler, midatauler);

		do {
			System.out.println("");
			System.out.println("Introdueix la fila de la casella que vols destapar: ");
			fila=validarDada(midatauler);
			System.out.println("Introdueix la columna de la casella que vols destapar: ");
			column=validarDada(midatauler);
			}
        while(!validarCasella(tauler, fila, column));

		tauler[fila][column]=secret[fila][column];
		mostrarTauler(tauler, midatauler);

		if(tauler[fil][col]==tauler[fila][column]) {
			System.out.println("Coincideixen!");
            continua = true;
		}
		else {
			tauler[fil][col]='?';
			tauler[fila][column]='?';
			System.out.println("No coincideixen...");
			continua = false;

		}
		return continua;
	}

	public static String Noms() {
		String nom;
		nom=reader.nextLine();
		return nom;
	}

	public static void joc() {
		final int MIDATAULER = 4;
	    char[][] tauler = new char[MIDATAULER][MIDATAULER];
        char[][] secret = new char[MIDATAULER][MIDATAULER];
		
		int punts1=0, punts2=0;
		boolean canvi=false;

		String nom1, nom2;
		System.out.println("Benvinguts al joc del Memory!");
		System.out.println("Introdueix el nom del jugador 1:");
		nom1=Noms();
		System.out.println("Introdueix el nom del jugador 2:");
		nom2=Noms();

		iniciarTauler(tauler, MIDATAULER);
		mostrarTauler(tauler, MIDATAULER);
		iniciaSecret(secret, MIDATAULER);

		while((punts1+punts2)<(MIDATAULER*2)) {

            if(!canvi) {
            System.out.println("Torn de "+nom1);
            if(torn(tauler,secret, MIDATAULER)) {
                punts1++;
            }else{
                    canvi=true;
                }
            }

            if(canvi) {
                System.out.println("Torn de "+nom2);
                if(torn(tauler,secret, MIDATAULER)) {
                    punts2++;
                }else {
                        canvi=false;
                }
            }
        }

        if(punts1>punts2) {
                System.out.println("El guanyador �s "+nom1+"!");
        }else if(punts2>punts1) {
                System.out.println("El guanyador �s "+nom2+"!");
        	  }
              else
                System.out.println("Empatats!!!");


	}

}
