package Solucio;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class BasquetManiaTestObtenirEquip {


	private int num1;
	private int num2;
	private char resul;
	
	public BasquetManiaTestObtenirEquip (int num1, int num2, char resul) {
		this.num1 = num1;
		this.num2 = num2;
		this.resul = resul;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{20, 10, 'L'},
			{30, 42, 'V'},
			{5, 5, 'E'}
		});
		
	}
	@Test
	public void testObtenirEquip() {
		char res = BasquetMania.obtenirEquip(num1,  num2);
		assertEquals(resul,  res);
	}
	

}
