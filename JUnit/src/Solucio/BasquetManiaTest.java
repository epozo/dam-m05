package Solucio;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BasquetManiaTest {

	@Test
	public void testObtenirEquip() {
		char res = BasquetMania.obtenirEquip(2, 3);
		
		assertEquals('V', res);
		//fail("Not yet implemented");
	}

	@Test
	public void testSumaPunts() {
		int [] resultats = {0, 0};
		String equip = "L";
		int punts = 3;
		int [] res = BasquetMania.sumaPunts(resultats, equip, punts);
		int [] esperat = {3, 0};
		assertArrayEquals(esperat, res);
		//fail("Not yet implemented");
	}

}
