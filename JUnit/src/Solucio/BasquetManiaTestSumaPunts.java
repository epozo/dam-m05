package Solucio;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class BasquetManiaTestSumaPunts {


	private int [] resultats;
	private String equip;
	private int punts;
	private int [] finals;
	
	private static int[] [] resini = { {0,1}, {0,0}, {1,3}, {3,1} };
	private static int [] [] resfi = { {1,1}, {0,3}, {2,3}, {3,3} };
	
	public BasquetManiaTestSumaPunts (int[] resultats, String equip, int punts, int [] finals) {
		this.resultats = resultats;
		this.equip = equip;
		this.punts = punts;
		this.finals = finals;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		
		return Arrays.asList(new Object[][] {
			{resini[0], "L", 1, resfi[0]},
			{resini[1], "V", 3, resfi[1]},
			{resini[2], "L", 1, resfi[2]},
			{resini[3], "V", 2, resfi[3]}
		});
		
	}
	

	@Test
	public void testSumaPunts() {
		int [] res = BasquetMania.sumaPunts(resultats, equip, punts);
		assertArrayEquals(finals,  res);
	}
	

}
