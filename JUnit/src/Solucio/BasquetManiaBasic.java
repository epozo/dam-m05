package Solucio;

import java.util.Scanner;

public class BasquetManiaBasic {
static Scanner reader = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String entrada;
		String [] valors;
		int casos = reader.nextInt();
		int puntsL, puntsV;
		char gana;
		
		String resultat;
		
		reader.nextLine();
		for ( ;casos > 0; casos--) {
			
			entrada = reader.nextLine();
			puntsL = 0;
			puntsV = 0;
			while (!entrada.equals("F")) {
				valors = entrada.split(" ");
				if (valors[0].equals("L")){
					puntsL = puntsL + Integer.parseInt(valors[1]);
				}
				else
					puntsV = puntsV + Integer.parseInt(valors[1]);	
				
				entrada = reader.nextLine();	
			}
			if (puntsL > puntsV)
				gana = 'L';
			else if (puntsV > puntsL)
					gana = 'V';
			else
				gana = 'E';
			System.out.println(gana + " " + puntsL + " " + puntsV);
			
			
		}	
	}
}
