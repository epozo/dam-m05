package Solucio;


import java.util.Scanner;

public class BasquetMania {

	static Scanner reader = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String entrada;
		String [] valors;
		int casos = reader.nextInt();
		int [] punts = {0, 0};
		char gana;
		
		String resultat;
		
		reader.nextLine();
		for ( ;casos > 0; casos--) {
			
			entrada = reader.nextLine();
			punts[0] = 0;
			punts[1] = 0;
			while (!entrada.equals("F")) {
				valors = entrada.split(" ");
				punts = sumaPunts(punts, valors[0], Integer.parseInt(valors[1]));
				
				entrada = reader.nextLine();	
			}
			gana = obtenirEquip(punts[0], punts[1]);
			
			System.out.println(gana + " " + punts[0] + " " + punts[1]);
			
			
			
		}
		
	}

	public static char obtenirEquip(int pl, int pv) {
		// TODO Auto-generated method stub
		char gana;
		if (pl > pv)
			gana = 'L';
		else if (pv > pl)
				gana = 'V';
		else
			gana = 'E';
		return gana;
	}

	public static int[] sumaPunts(int[] resultats, String equip, int punts) {
		// TODO Auto-generated method stub
		if (equip.equals("L")){
			resultats[0] = resultats[0] + punts;
		}
		else
			resultats[1] = resultats[1] + punts;
		
		return resultats;
	}

}
