package calculadora;

import static org.junit.Assert.*;
import org.junit.Test;

class LaCalculadoraTest {

	@Test
	void testSuma() {
		int res = Calculadora.suma(10,  20);
		assertEquals(30,  res);
	//	fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = Calculadora.resta(10,  20);
		assertEquals(-10,  res);
	//	fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = Calculadora.multiplicacio(10,  20);
		assertEquals(200,  res);
	//	fail("Not yet implemented");
	}

	@Test
	void testDivisio() {
		int res = Calculadora.divisio(10, 5);
		assertEquals(2,  res);
	//	fail("Not yet implemented");
	}

}
